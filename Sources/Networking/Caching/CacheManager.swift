//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//
  

import Foundation
/// A class to manage caching of URL responses.
public class CacheManager {
    // Singleton instance of the CacheManager.
    static let shared = CacheManager()
    
    // URLCache instance to store and retrieve cached responses.
    private let cache = URLCache.shared

    /// Stores a URL response along with its data in the cache.
    ///
    /// - Parameters:
    ///   - request: The URLRequest for which the response is being cached.
    ///   - data: The Data associated with the response.
    ///   - response: The URLResponse to be cached.
    func cacheResponse(for request: URLRequest, data: Data, response: URLResponse) {
        if let url = request.url {
            let cachedResponse = CachedURLResponse(response: response, data: data)
            cache.storeCachedResponse(cachedResponse, for: URLRequest(url: url))
        }
    }

    /// Retrieves a cached URL response for a given URLRequest.
    ///
    /// - Parameter request: The URLRequest for which the cached response is required.
    /// - Returns: The CachedURLResponse if available, otherwise nil.
    func cachedResponse(for request: URLRequest) -> CachedURLResponse? {
        if let url = request.url {
            return cache.cachedResponse(for: URLRequest(url: url))
        }
        return nil
    }
    
    /*
     let url = URL(string: "https://api.example.com/data")!
     let request = URLRequest(url: url)

     // Get cached response for the request
     if let cachedResponse = CacheManager.shared.cachedResponse(for: request) {
         // Use cached response data
         print("Cached data: \(cachedResponse.data)")
     } else {
         // Fetch data from the server and cache the response
         let task = URLSession.shared.dataTask(with: request) { data, response, error in
             if let data = data, let response = response {
                 CacheManager.shared.cacheResponse(for: request, data: data, response: response)
             }
         }
         task.resume()
     }
     */
}
