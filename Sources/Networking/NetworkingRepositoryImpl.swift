//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import Foundation
import Combine

// MARK: CORE
public final class NetworkingRepositoryImpl: NetworkingRepository {
    private let session: URLSession
    
    private let queue = DispatchQueue(label: "RequestThrottlerQueue")
    private let maxRequestsPerInterval: Int
    private let interval: TimeInterval
    private var requestCount: Int = 0
    private var lastResetDate: Date

    init(session: URLSession = .shared, maxRequestsPerInterval: Int, interval: TimeInterval) {
        self.session = session
        self.maxRequestsPerInterval = maxRequestsPerInterval
        self.interval = interval
        self.lastResetDate = Date()
    }
    
    public func fetchData<T: Decodable>(from url: URL, decodingType: T.Type) -> AnyPublisher<T, Error> {
        session.dataTaskPublisher(for: url)
            .tryMap { data, response -> Data in
                guard let httpResponse = response as? HTTPURLResponse,
                      200...299 ~= httpResponse.statusCode else {
                    throw URLError(.badServerResponse)
                }
                return data
            }
            .decode(type: decodingType, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
    
    public func fetchData<T: Decodable>(from url: URL, decodingType: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        session.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(error ?? URLError(.unknown)))
                return
            }
            do {
                let decodedData = try JSONDecoder().decode(decodingType, from: data)
                completion(.success(decodedData))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
    
    public func downloadFile(from url: URL, completion: @escaping (Result<URL, Error>) -> Void) {
        session.downloadTask(with: url) { url, response, error in
            guard let tempURL = url, error == nil else {
                completion(.failure(error ?? URLError(.unknown)))
                return
            }
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let destinationURL = documentsURL.appendingPathComponent(url?.lastPathComponent ?? "file")
            do {
                try FileManager.default.moveItem(at: tempURL, to: destinationURL)
                completion(.success(destinationURL))
            } catch let error {
                completion(.failure(error))
            }
        }.resume()
    }
}

// MARK: DNS RESOLVER
public extension NetworkingRepositoryImpl {
    /// Resolves the IP addresses associated with a given domain name.
    ///
    /// - Parameters:
    ///   - domain: The domain name to resolve.
    ///   - completion: A completion handler to be called once the resolution has completed.
    ///                 Returns a list of IP addresses on success or an Error on failure.
    func resolveDNS(domain: String, completion: @escaping (Result<[String], Error>) -> Void) {
        // Create a CFHost instance for the provided domain name
        let host = CFHostCreateWithName(nil, domain as CFString).takeRetainedValue()
        
        // Start the asynchronous DNS resolution process
        CFHostStartInfoResolution(host, .addresses, nil)
        
        var success: DarwinBoolean = false
        
        // Retrieve the resolved IP addresses if the resolution was successful
        if let addresses = CFHostGetAddressing(host, &success)?.takeUnretainedValue() as NSArray? {
            var results: [String] = []
            
            // Iterate through the resolved IP addresses
            for case let address as NSData in addresses {
                // Convert the sockaddr structure to a human-readable IP address string
                var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                if getnameinfo(address.bytes.assumingMemoryBound(to: sockaddr.self), socklen_t(address.length),
                               &hostname, socklen_t(hostname.count), nil, 0, NI_NUMERICHOST) == 0 {
                    let ipAddress = String(cString: hostname)
                    results.append(ipAddress)
                }
            }
            
            // Call the completion handler with the list of IP addresses
            completion(.success(results))
        } else {
            // Call the completion handler with an error if the resolution failed
            completion(.failure(NSError(domain: "DNSResolutionError", code: 1, userInfo: nil)))
        }
    }
    
    /*
     resolveDNS(domain: "example.com") { result in
     switch result {
     case .success(let ipAddresses):
     print("Resolved IP addresses: \(ipAddresses)")
     case .failure(let error):
     print("Error resolving domain: \(error.localizedDescription)")
     }
     }
     */
}

// MARK: Network Diagnostics - Connectivity Check
public extension NetworkingRepositoryImpl {
    /// Checks internet connectivity by sending a request to a known website (e.g., Google).
    ///
    /// - Parameter completion: A completion handler to be called once the connectivity check has completed.
    ///                         Returns `true` if connected to the internet, `false` otherwise.
    func checkConnectivity(completion: @escaping (Bool) -> Void) {
        // Define a URL for a known website (Google in this case)
        let url = URL(string: "https://www.google.com")!
        
        // Create a URLSession data task to send a request to the specified URL
        let task = URLSession.shared.dataTask(with: url) { (_, response, error) in
            // Call the completion handler with the result of the connectivity check
            // If the response is not nil and there is no error, the device is connected to the internet
            completion(response != nil && error == nil)
        }
        
        // Resume the data task to start the request
        task.resume()
    }
    
    /*
     checkConnectivity { isConnected in
     if isConnected {
     print("Connected to the internet")
     } else {
     print("Not connected to the internet")
     }
     }
     
     */
}

// MARK: Connection Retry Mechanism
public extension NetworkingRepositoryImpl {
    /// Sends an HTTP request with retry capabilities in case of a failure.
    ///
    /// - Parameters:
    ///   - url: The URL to send the request to.
    ///   - retries: The number of times to retry the request in case of failure.
    ///   - delay: The time interval in seconds to wait before retrying the request.
    ///   - completion: A completion handler to be called once the request has completed.
    ///                 Returns the response data, URL response, and error (if any).
    func sendRequestWithRetry(url: URL, retries: Int, delay: TimeInterval, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error, retries > 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [self] in
                    self.sendRequestWithRetry(url: url, retries: retries - 1, delay: delay, completion: completion)
                }
            } else {
                completion(data, response, error)
            }
        }
        task.resume()
    }
    
    /*
     
     */
}

// MARK: Connection Retry Mechanism
public extension NetworkingRepositoryImpl {
    /// Executes a request, taking into account the throttling constraints.
    ///
    /// - Parameters:
    ///   - request: The URLRequest to be executed.
    ///   - completion: A completion handler to be called once the request has completed.
    ///                 Returns the response data, URL response, and error (if any).
    func executeRequest(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        queue.sync {
            if requestCount >= maxRequestsPerInterval {
                let elapsedTime = Date().timeIntervalSince(lastResetDate)
                if elapsedTime >= interval {
                    resetRequestCount()
                } else {
                    let delay = interval - elapsedTime
                    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                        self.executeRequest(request: request, completion: completion)
                    }
                    return
                }
            }
            
            requestCount += 1
            let task = URLSession.shared.dataTask(with: request, completionHandler: completion)
            task.resume()
        }
    }
    
    /// Resets the request count to 0 and updates the last reset date.
    private func resetRequestCount() {
        requestCount = 0
        lastResetDate = Date()
    }
}

// MARK: LOGGING Request / Response
public extension NetworkingRepositoryImpl {

    func logRequest(request: URLRequest) {
        print("Request URL: \(request.url?.absoluteString ?? "Unknown")")
        print("Request Method: \(request.httpMethod ?? "Unknown")")
        print("Request Headers: \(request.allHTTPHeaderFields ?? [:])")
        if let body = request.httpBody, let bodyString = String(data: body, encoding: .utf8) {
            print("Request Body: \(bodyString)")
        }
    }

    func logResponse(response: URLResponse, data: Data?, error: Error?) {
        print("Response URL: \(response.url?.absoluteString ?? "Unknown")")
        if let httpResponse = response as? HTTPURLResponse {
            print("Response Status Code: \(httpResponse.statusCode)")
            print("Response Headers: \(httpResponse.allHeaderFields)")
        }
        if let data = data, let bodyString = String(data: data, encoding: .utf8) {
            print("Response Body: \(bodyString)")
        }
        if let error = error {
            print("Response Error: \(error.localizedDescription)")
        }
    }
}
