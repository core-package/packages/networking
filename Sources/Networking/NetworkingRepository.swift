//
//
//  Copyright © 2023.
//  Alon Yakobichvili
//  All rights reserved.
//

import Foundation
import Combine

public protocol NetworkingRepository {
    func fetchData<T: Decodable>(from url: URL, decodingType: T.Type) -> AnyPublisher<T, Error>
    func fetchData<T: Decodable>(from url: URL, decodingType: T.Type, completion: @escaping (Result<T, Error>) -> Void)
    func downloadFile(from url: URL, completion: @escaping (Result<URL, Error>) -> Void)
    
    // MARK: Network Diagnostics - Connectivity Check
    func checkConnectivity(completion: @escaping (Bool) -> Void)
    
    // MARK: Connection Retry Mechanism
    func executeRequest(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
    
    // MARK: Connection Retry Mechanism
    func sendRequestWithRetry(url: URL, retries: Int, delay: TimeInterval, completion: @escaping (Data?, URLResponse?, Error?) -> Void)
    
    // MARK: LOGGING Request / Response
    func logRequest(request: URLRequest)
    func logResponse(response: URLResponse, data: Data?, error: Error?)
}
