import XCTest
import Combine
@testable import Networking

class NetworkingLibraryTests: XCTestCase {
    var networkingRepository: NetworkingRepository!
    var mockSession: MockURLSession!
    var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        super.setUp()
        cancellables = []
        mockSession = MockURLSession()
        networkingRepository = MockNetworkingRepository(mockSession: mockSession)
    }
    
    override func tearDown() {
        mockSession = nil
        networkingRepository = nil
        cancellables = nil
        super.tearDown()
    }
    
    func testFetch() {
        let expectation = XCTestExpectation(description: "Fetch data")
        let url = URL(string: "https://example.com")!
        let testData = Data("""
        {
            "name": "John Doe",
            "age": 42
        }
        """.utf8)
        mockSession.mockData = testData
        
        networkingRepository.fetchData(from: url, decodingType: User.self)
            .sink { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    XCTFail("Fetch failed with error: \(error)")
                }
                expectation.fulfill()
            } receiveValue: { user in
                XCTAssertEqual(user.name, "John Doe")
                XCTAssertEqual(user.age, 42)
            }
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFetchWithResultHandler() {
        let expectation = XCTestExpectation(description: "Fetch data")
        let url = URL(string: "https://example.com")!
        let testData = Data("Test Data".utf8)
        mockSession.mockData = testData
        
        networkingRepository.fetchData(from: url, decodingType: String.self) { result in
            switch result {
            case .success(let data):
                XCTAssertEqual(data, "Test Data")
            case .failure(let error):
                XCTFail("Fetch failed with error: \(error)")
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testFetchWithInvalidURL() {
        let expectation = XCTestExpectation(description: "Fetch data")
        expectation.isInverted = true
        
        let url = URL(string: "www.google.com")!
        let testData = Data("Test Data".utf8)
        mockSession.mockData = testData
        
        networkingRepository.fetchData(from: url, decodingType: String.self)
            .sink { completion in
                switch completion {
                case .finished:
                    XCTFail("Fetch should have failed with invalid URL")
                case .failure(let error):
                    XCTAssertEqual(error.localizedDescription, "unsupported URL")
                }
                expectation.fulfill()
            } receiveValue: { data in
                XCTFail("Should not receive any value")
            }
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testDownload() {
        let expectation = XCTestExpectation(description: "Download file")
        let url = URL(string: "https://example.com/file.pdf")!
        let testFileData = Data(count: 1024)
        mockSession.mockData = testFileData
        
        networkingRepository.downloadFile(from: url) { result in
            switch result {
            case .success(let fileURL):
                let receivedFileData = try! Data(contentsOf: fileURL)
                XCTAssertGreaterThan(receivedFileData.count, 0, "Downloaded file should not be empty")
            case .failure(let error):
                XCTFail("Download failed with error: \(error)")
            }
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    
    public class MockNetworkingRepository: NetworkingRepository {
        private let mockSession: MockURLSession
        var cancellables: Set<AnyCancellable>!
        
        init(mockSession: MockURLSession) {
            self.mockSession = mockSession
            self.cancellables = []
        }
        
        public func fetchData<T>(from url: URL, decodingType: T.Type, completion: @escaping (Result<T, Error>) -> Void) where T : Decodable {
            let publisher = fetchData(from: url, decodingType: T.self)
            publisher.sink(receiveCompletion: { result in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .finished:
                    break
                }
            }, receiveValue: { data in
                completion(.success(data))
            })
            .store(in: &cancellables)
        }
        
        public func fetchData<T>(from url: URL, decodingType: T.Type) -> AnyPublisher<T, Error> where T : Decodable {
            let dataTaskPublisher = URLSession.DataTaskPublisher(request: URLRequest(url: url), session: mockSession)
            
            let publisher = dataTaskPublisher
                .tryMap { output -> Data in
                    guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                        throw URLError(.badServerResponse)
                    }
                    return output.data
                }
                .decode(type: decodingType, decoder: JSONDecoder())
                .eraseToAnyPublisher()
            
            return publisher
        }
        
        public  func downloadFile(from url: URL, completion: @escaping (Result<URL, Error>) -> Void) {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let mockFileURL = documentsURL.appendingPathComponent("mockfile.pdf")
            
            // Create a mock PDF file with blank data
            let data = Data(count: 1024)
            do {
                try data.write(to: mockFileURL)
                completion(.success(mockFileURL))
            } catch let error {
                completion(.failure(error))
            }
        }
        
        public func checkConnectivity(completion: @escaping (Bool) -> Void) {
            completion(false)
        }
        
        public func executeRequest(request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
            completion(nil, nil, nil)
        }
        
        public func sendRequestWithRetry(url: URL, retries: Int, delay: TimeInterval, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
            completion(nil, nil, nil)
        }
        
        public func logRequest(request: URLRequest) {
            print("loaded")
        }
        
        public func logResponse(response: URLResponse, data: Data?, error: Error?) {
            print("loaded")
        }
        
        
        let mockData = Data("""
        {
            "name": "John Doe",
            "age": 42
        }
    """.utf8)
        
        
    }
    
    public struct User: Codable {
        let name: String
        let age: Int
    }
    
    public class MockURLSession: URLSession {
        var mockData: Data?
        var mockError: Error?
        
        public override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            let mockDataTask = MockURLSessionDataTask()
            mockDataTask.mockData = mockData
            mockDataTask.mockError = mockError
            mockDataTask.completionHandler = completionHandler
            return mockDataTask
        }
    }
    
    public class MockURLSessionDataTask: URLSessionDataTask {
        var mockData: Data?
        var mockError: Error?
        var completionHandler: ((Data?, URLResponse?, Error?) -> Void)?
        override func resume() {
            DispatchQueue.global().async {
                self.completionHandler?(self.mockData, nil, self.mockError)
            }
        }
    }
}
